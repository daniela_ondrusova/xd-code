# XD - Code

**Features**

- Gedetailleerde overname van alle XD-ontwerpkenmerken naar CSS.
- Inclusief XD-eigenschappen zoals border-radius, box-shadows, lettertype, tekstkleur, achtergrondkleuren, padding en marges.

Bekijk de XD-Code [hier](https://super-brioche-e97a0d.netlify.app/).